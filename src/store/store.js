import { createStore, applyMiddleware, combineReducers } from "redux";
import createSagaMiddleware from "redux-saga";
import { authReducer } from "./auth/reducer";
import { watcherLogin, watcherLogout } from "./auth/saga";
import { all } from 'redux-saga/effects';

const rootReducer = combineReducers({
  auth: authReducer,
});

const sagaMiddleware = createSagaMiddleware();

function* rootSaga() {
  yield all([watcherLogin(), watcherLogout()]);
}

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default store;
