import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_FAILURE } from "./actions";

const initialState = 0;

export const authReducer = (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case LOGIN_SUCCESS:
      return state + 1;
    case LOGOUT_SUCCESS:
      return state - 1;
    default:
      return state;
  }
};
