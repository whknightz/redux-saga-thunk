import { takeLatest, call, put } from "redux-saga/effects";
import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_FAILURE } from "./actions";

const delay = (ms) => new Promise((res) => setTimeout(res, ms));

function* workerLogin(payload) {
  console.log("login", payload);
  try {
    yield delay(300);
    // const response = yield call(login);
    // const data = response.data;
    yield put({ type: LOGIN_SUCCESS, payload: { username: "Khanh" } });
  } catch (error) {
    yield put({ type: LOGIN_FAILURE, error });
  }
}

function* workerLogout() {
  console.log("logout");
  try {
    yield delay(1000);
    // const response = yield call(login);
    // const data = response.data;
    yield put({ type: LOGOUT_SUCCESS, payload: { username: "Khanh" } });
  } catch (error) {
    yield put({ type: LOGOUT_FAILURE, error });
  }
}

export function* watcherLogin() {
  yield takeLatest(LOGIN_REQUEST, workerLogin);
}

export function* watcherLogout() {
  yield takeLatest(LOGOUT_REQUEST, workerLogout);
}
